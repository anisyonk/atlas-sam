#!/bin/bash

errorFile=/tmp/voFeedPrs_error.txt
echo `date` "START" > $errorFile
if [ -e /usr/libexec/grid-monitoring/probes/org.atlas/voFeedParser.py ];
then
    python /usr/libexec/grid-monitoring/probes/org.atlas/voFeedParser.py >> $errorFile 2>&1
else
    python /afs/cern.ch/user/s/stupputi/public/ADC/atlasNagios/etf-dev/org.atlas/extras/voFeedParser.py >> $errorFile 2>&1
fi

if [ $? -ne 0 ];
then
  echo `date` "FINISHED" >> $errorFile
  echo "Script failed"
  cat $errorFile | mail -s "Cron job VO feed from AGIS failed" ryu.sawada@cern.ch
fi

if [ ! -s /var/www/html/filtered_VOFeed_tmp.txt ];
then
  rm -f /var/www/html/filtered_VOFeed_tmp.txt
  #echo "Generated file is empty. Check error file at $errorFile." | mail -s "Cron job DDM endpoints from AGIS failed" ryu.sawada@cern.ch 
else
  mv /var/www/html/filtered_VOFeed_tmp.txt /var/www/html/filtered_VOFeed.txt
fi
