%define _unpackaged_files_terminate_build 0
%define _missing_doc_files_terminate_build 0
%define site org.atlas
%define dir %{_libexecdir}/grid-monitoring/probes/%{site}
%define dirlcg %{_libexecdir}/grid-monitoring/probes/%{sitelcg}
%define dirpilot %{_libexecdir}/grid-monitoring/probes/%{sitepilot}
#%define ncgconf /etc/ncg-metric-config.d
#%define ncgdb /etc/ncg/ncg-localdb.d
%define cronDaily /etc/cron.daily
#%define cronHourly /etc/cron.hourly
%define debug_package %{nil}
%define www /var/www/html
%define ncgx /usr/lib/ncgx/x_plugins

Summary: WLCG Compliant Probes from %{site}
Name: nagios-plugins-wlcg-org.atlas
Version: 0.2.11
Release: 1%{?dist}

License: ASL 2.0
Group: Network/Monitoring
Source0: %{name}-%{version}.tgz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root
#Requires:
AutoReqProv: no
BuildArch: noarch
#BuildRequires: python >= 2.3
Requires: python-argparse >= 1.2.1
Requires: python-lxml >= 2.2.3
Requires: gfal2 >= 2.9.3
Requires: gfal2-all >= 2.9.3
#Requires: gfal2-core >= 2.8.1
#Requires: gfal2-plugin-dcap >= 2.8.1
#Requires: gfal2-plugin-gridftp >= 2.8.1
#Requires: gfal2-plugin-http >= 2.8.1
#Requires: gfal2-plugin-lfc >= 2.8.1
#Requires: gfal2-plugin-rfio >= 2.8.1
#Requires: gfal2-plugin-srm >= 2.8.1
Requires: gfal2-plugin-xrootd >= 0.4.0
Requires: gfal2-python >= 1.8.3
#Requires: gfal2-python-doc >= 1.7.0
#Requires: gfal2-transfer >= 2.8.1
#Requires: gfal2-util.noarch >= 1.0.0

%description
TODO

%prep
%setup -q

%build

%install
export DONT_STRIP=1
%{__rm} -rf %{buildroot}
install --directory %{buildroot}/%{dir}
install --directory %{buildroot}/%{cronDaily}
install --directory %{buildroot}/%{www}
install --directory %{buildroot}/%{ncgx}
#install --directory %{buildroot}/%{cronHourly}
#install --directory %{buildroot}/%{ncgconf}
#install --directory %{buildroot}/%{ncgdb}
%{__cp} -rpf ./src/wnjob  %{buildroot}/%{dir}
%{__cp} -rpf ./src/DDM  %{buildroot}/%{dir}
#%{__cp} -rpf ./nagios_machine_cfg/atlas.conf %{buildroot}/%{ncgconf}
#%{__cp} -rpf ./nagios_machine_cfg/emi.ce.CREAMCE.conf %{buildroot}/%{ncgconf}
#%{__cp} -rpf ./nagios_machine_cfg/atlas_param_override %{buildroot}/%{ncgdb}
%{__cp} -rpf ./extras/getAGISATLASInfo.sh %{buildroot}/%{cronDaily}
%{__cp} -rpf ./extras/getAGISATLASInfo.py %{buildroot}/%{dir}
%{__cp} -rpf ./extras/voFeedParser.sh %{buildroot}/%{cronDaily}
%{__cp} -rpf ./extras/voFeedParser.py %{buildroot}/%{dir}
%{__cp} -rpf ./extras/filtered_VOFeed.txt %{buildroot}/%{www}
%{__cp} -rpf ./extras/vofeed_atlas.py %{buildroot}/%{ncgx}
#%{__cp} -rpf ./extras/webdav_atlas.py %{buildroot}/%{ncgx}

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-,root,root,-)
%{dir}
#%{ncgconf}
#%{ncgconf}/atlas.conf
#%{ncgconf}/emi.ce.CREAMCE.conf
#%{ncgdb}
#%{ncgdb}/atlas_param_override
%{cronDaily}/getAGISATLASInfo.sh
%{cronDaily}/voFeedParser.sh
%{www}/filtered_VOFeed.txt
%{ncgx}/vofeed_atlas.py
#%{ncgx}/webdav_atlas.py

%changelog
* Wed Sep 07 2016 R. Sawada <ryu.sawada@cern.ch> - 0.2.2-17
- Add host group
* Wed May 11 2016 S. A. Tupputi <salvatore.a.tupputi@cnaf.infn.it> - 0.2.2-16
- Removed FrontierSquid test from atlas.conf and substituted WARNING with CRITICAL in case gfal2 test time out for connection problems
* Fri May 06 2016 S. A. Tupputi <salvatore.a.tupputi@cnaf.infn.it> - 0.2.2-15
- Timeouts/connection problems switched from CRITICAL from WARNING
* Wed May 04 2016 S. A. Tupputi <salvatore.a.tupputi@cnaf.infn.it> - 0.2.2-14
- Added unique_tag field in webdav file
* Tue May 03 2016 S. A. Tupputi <salvatore.a.tupputi@cnaf.infn.it> - 0.2.2-13
- Extended srm and connection timeouts
* Thu Apr 28 2016 S. A. Tupputi <salvatore.a.tupputi@cnaf.infn.it> - 0.2.2-12
- Added new webdav TLS_CIPHERS metric in webdav_atlas.py file
* Thu Apr 28 2016 S. A. Tupputi <salvatore.a.tupputi@cnaf.infn.it> - 0.2.2-11
- Extended ad hoc LsDir setting to all eos endpoints
* Thu Apr 28 2016 S. A. Tupputi <salvatore.a.tupputi@cnaf.infn.it> - 0.2.2-10
- Inserted ad hoc LsDir setting for srm-eosatlas
* Tue Apr 26 2016 S. A. Tupputi <salvatore.a.tupputi@cnaf.infn.it> - 0.2.2-9
- Changed timeout  values
* Mon Apr 18 2016 S. A. Tupputi <salvatore.a.tupputi@cnaf.infn.it> - 0.2.2-7
- Removed python timeouts and enabled srm plugin timeouts (via gfal2)
* Tue Apr 05 2016 S. A. Tupputi <salvatore.a.tupputi@cnaf.infn.it> - 0.2.2-5
- modifications to previous transient release
* Fri Mar 25 2016 S. A. Tupputi <salvatore.a.tupputi@cnaf.infn.it> - 0.2.2-4
- transient release before moving to etf-compliant build
* Tue Oct 27 2015 S. A. Tupputi <salvatore.a.tupputi@cnaf.infn.it> - 0.2.2-2
- disabled localfileaccess execution and modifed swspace to reduce output
* Fri Oct 23 2015 S. A. Tupputi <salvatore.a.tupputi@cnaf.infn.it> - 0.2.2-1
- fixed dependency and DDM-probe string treatment for first experience on etf-atlas-dev
* Wed Sep 30 2015 S. A. Tupputi <salvatore.a.tupputi@cnaf.infn.it> - 0.2.1-19
- optimization of LsDir for https 
* Mon Aug 24 2015 S. A. Tupputi <salvatore.a.tupputi@cnaf.infn.it> - 0.2.1-14
- Added parser for filtered VO feed
* Wed Aug 19 2015 S. A. Tupputi <salvatore.a.tupputi@cnaf.infn.it> - 0.2.1-11
- Drop old SRM probes and full switch to gfal2 probes for srm, https, xrootd protocols
* Mon Jun 09 2015 S. A. Tupputi <salvatore.a.tupputi@cnaf.infn.it> - 0.2.0-8
- Changes to status assignment for some branching for DDM probe before going to production
* Mon Feb 16 2015 S. A. Tupputi <salvatore.a.tupputi@cnaf.infn.it> - 0.2.0-6
- Old SRM probes along with new DDM ones for ultimate testing before going to prod - improved version and /var dirs creation included in probe
* Thu Jan 29 2015 S. A. Tupputi <salvatore.a.tupputi@cnaf.infn.it> - 0.2.0-3
- Old SRM probes along with new DDM ones for ultimate testing before going to prod
* Tue Oct 18 2014 S. A. Tupputi <salvatore.a.tupputi@cnaf.infn.it> - 0.2.0-1
- Integration of gfal2-based SE tests with current CE tests. Number progression starting from prod package.
* Mon Apr 17 2014 S. A. Tupputi <salvatore.a.tupputi@cnaf.infn.it> - 0.0.1-1
- Initial build
